﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Interfaces;
using DAL;

namespace BLL.Implementations
{
    public class TourService : ITourService
    {
        private readonly ApplicationDbContext db;
        public TourService(ApplicationDbContext context)
        {
            db = context;
        }
        public async Task AddTourToUserAsync(string userId, int tourId)
        {
            var tour = await db.Tours.FirstOrDefaultAsync(s => s.Id == tourId);
            if (tour != null)
            {
                db.UserTours.Add(new UserTour
                {
                    BookDate = DateTime.Now,
                    UserId = userId,
                    TourId = tourId,
                    PriceWhenOrdered = tour.Price

                });
                await db.SaveChangesAsync();
            }
        }
    }
}
