﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using DAL.Migrations;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DAL
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public List<UserTour> UserTours { get; set; }
    }


    public class Tour
    {
        public int Id { get; set; }
        [Display(Name = "Страна")]
        public string Country { get; set; }
        [Display(Name = "Категория")]
        public string Category { get; set; }
        [Display(Name = "Вылет Из")]
        public string Fly { get; set; }
        [Display(Name = "Отель, в который заселят")]
        public string Hotel { get; set; }
        [Display(Name = "Дата начала")]
        public DateTime StartDate { get; set; }
        [Display(Name = "Дата конца")]
        public DateTime FinishDate { get; set; }
        [Display(Name = "Тип еды")]
        public string Food { get; set; }
        [Display(Name = "Число мест для взрослых")]
        public int NumberPlacesForOld { get; set; }
        [Display(Name = "Число мест для детей")]
        public int NumberPlacesForChildren { get; set; }
        [Display(Name = "Цена")]
        public double Price { get; set; }
    }

    public class UserTour
    {
        public int Id { get; set; }
        public int TourId { get; set; }
        public Tour Tour { get; set; }
        public string UserId { get; set; }
        public bool IsCanceled { get; set; }
        public double PriceWhenOrdered { get; set; }
        public DateTime BookDate { get; set; }
        public ApplicationUser User { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        private static readonly object Lock = new object();
        private static bool _databaseInitialized;
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {

            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;

            if (_databaseInitialized)
            {
                return;
            }
            lock (Lock)
            {
                if (!_databaseInitialized)
                {
                    Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationDbContext, Configuration>());
                    _databaseInitialized = true;
                }
            }
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<Tour> Tours { get; set; }
        public DbSet<UserTour> UserTours { get; set; }
    }
}