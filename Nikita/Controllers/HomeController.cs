﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nikita.Models;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Threading.Tasks;
using BLL.Interfaces;
using DAL;
using Microsoft.AspNet.Identity;

namespace Nikita.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ITourService _tourService;
        private readonly ApplicationDbContext db;

        public HomeController(ITourService tourService, ApplicationDbContext ctx)
        {
            _tourService = tourService;
            db = ctx;
        }
        public ActionResult Index()
        {
            ToursIndexModel model = new ToursIndexModel
            {
                AvailableCategories = db.Tours.Select(s => s.Category).Distinct().ToList(),
                AvailableCountries = db.Tours.Select(s => s.Country).Distinct().ToList(),
                AvailableFlies = db.Tours.Select(s => s.Fly).Distinct().ToList(),
                AvailableFood = db.Tours.Select(s => s.Food).Distinct().ToList()
            };
            return View(model);
        }

        public ActionResult SearchTours(SearchTourRequest request)
        {
            IEnumerable<Tour> result;
            if (!ModelState.IsValid || request == null)
            {
                result = new List<Tour>();
                return PartialView(result);
            }
            result = db.Tours;
            if (request.Categories != null)
            {
                result = result.Where(s => request.Categories.Any(a => a == s.Category));
            }
            if (request.Countries != null)
            {
                result = result.Where(s => request.Countries.Any(a => a == s.Country));
            }
            if (request.Flies != null)
                result = result.Where(s => request.Flies.Any(a => a == s.Fly));
            if (request.StartDate.HasValue)
            {
                result = result.Where(s => s.StartDate == request.StartDate);
            }
            if (request.FinishDate.HasValue)
            {
                result = result.Where(s => s.FinishDate == request.FinishDate);
            }
            if (request.Food != null)
                result = result.Where(s => request.Food.Any(a => a == s.Food));
            if (request.Hotel != null)
                result = result.Where(s => s.Hotel.Contains(request.Hotel));
            if (request.NumberPlacesForChildren != null)
                result = result.Where(s => s.NumberPlacesForChildren >= request.NumberPlacesForChildren);
            if (request.NumberPlacesForOld != null)
                result = result.Where(s => s.NumberPlacesForOld >= request.NumberPlacesForOld);
            if (request.MinPrice != null)
                result = result.Where(s => s.Price >= request.MinPrice);
            if (request.MaxPrice != null)
                result = result.Where(s => s.Price <= request.MaxPrice).ToList();
            return PartialView(result);
        }

        public async Task<ActionResult> AddTourToUser(int tourId)
        {
            var userId = User.Identity.GetUserId<string>();
            await _tourService.AddTourToUserAsync(userId, tourId);
            return RedirectToAction("UserTours", "Account");
        }

    }

    public class ToursIndexModel
    {
        public List<string> AvailableCountries { get; set; }
        public List<string> AvailableCategories { get; set; }
        public List<string> AvailableFlies { get; set; }
        public List<string> AvailableFood { get; set; }
    }

    public class SearchTourRequest
    {
        public List<string> Countries { get; set; }
        public List<string> Categories { get; set; }
        public List<string> Flies { get; set; }
        public string Hotel { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? FinishDate { get; set; }
        public List<string> Food { get; set; }
        public int? NumberPlacesForOld { get; set; }
        public int? NumberPlacesForChildren { get; set; }
        public double? MinPrice { get; set; }
        public double? MaxPrice { get; set; }
    }
}