﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Nikita.Startup))]
namespace Nikita
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
